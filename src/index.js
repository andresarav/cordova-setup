import React from 'react';
import { render } from 'react-dom';
import './index.css'
import App from './App';
// import * as serviceWorker from './serviceWorker';

const startApp = () => {
  const home = document.getElementById('root')
  try {
    render(
      <App />
      , home
    );
  } catch (error) {
    // TODO: maneja el error
  }
  // TODO: tenemos SW?
  // registerServiceWorker()
}

if (!window.cordova) {
  startApp()
} else {
  document.addEventListener('deviceready', startApp, false)
  alert('whit cordova')
}

// serviceWorker.unregister();
