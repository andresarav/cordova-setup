import React, { useRef, Fragment } from 'react'
import InifiniteScroll from './components/infiniteScroll'

const App = () => {
  return (
    <InifiniteScroll/>
  );
}

export default App
