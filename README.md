1. install cordova with npm: "sudo npm install -g cordova"

2. create a cordova project: "cordova create [project_name]""
2.1 enter to project: cd [project_name]

3.the comand: "cordova platform" allows to see also installed and available platforms

4.add android platform: "cordova platform add android"

 > ====>  INSTALL PRE REQUISITES FOR BUILDING <====

 1. execute the command: "cordova requirements"

 > Install android platform requirements ==>

1. Install java JDK 8: "sudo apt-get install openjdk-8-jdk"
1.1 How uninstall java: https://novicestuffs.wordpress.com/2017/04/25/how-to-uninstall-java-from-linux/

2. Install Gradle: "sudo apt-get install gradle"

3. Install Android Studio:
3.1 Download: https://developer.android.com/studio/index.html
3.2 Move [Downloaded_file] to /opt/ folder: "mv [Downloaded_file.tar.gz] /opt/""
3.3 Uncompress file: tar -xzvf [Downloaded_file.tar.gz]

4.Execute Android Studio: Into the android studio folder, enter to /bin and execute ./studio.sh
4.1. Open the Android SDK Manager (Tools > SDK Manager in Android Studio, or sdkmanager on the command line), and make sure the following are installed:
* Android Platform SDK for your targeted version of Android
* Android SDK build-tools version 19.1.0 or higher
* Android Support Repository (found under the "SDK Tools" tab)


> ====> Setting environment variables <====

1. Declare the variables into ~/.bashrc: "sudo nano ~/.bashrc"

2. Declare this:

      JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
      PATH=$PATH:$HOME/bin:$JAVA_HOME/bin
      export JAVA_HOME
      export JRE_HOME
      export PATH

      export ANDROID_HOME=/home/[user_name]/Android/Sdk
      export PATH=$PATH:$ANDROID_HOME/tools
      export PATH=$PATH:$ANDROID_HOME/platform-tools

3. Setear the bash file:  "source ~/.bashrc"

>  ====> Execute build on cordova <=====

1. Command: "cordova build android"




>  ====> Integrate cordova to react CRA <=====

https://github.com/johnkmzhou/cordova-create-react-app


> =====> DEBUG BUILD IN CORDOVA ANDROID <=======

https://geeklearning.io/apache-cordova-and-remote-debugging-on-android/







































<!--  -->
